# Stylechecker Backend
## CO600 Group Project (jb695, jtk6, cbag2, ms693)

This is the Java backend of the code style checker for the 1st year module CO520

This project uses [Maven](https://maven.apache.org/) to manage its dependencies.

To build the project from source:

1. Clone the Git repository

2. Update the frontend assets configuration file located at `src\main\resources\assets\frontend\config.json`

2. With Maven installed on your machine and available on your PATH run: `mvn clean package -DskipTests` from the project root directory

3. This will put a `stylechecker-backend-1.0-SNAPSHOT.jar` in the `target` folder


To run the backend software:

1. Create a configuration file and example can be found in `src\main\resources\stylechecker-config.yml`

2. Run the following to start the server `java -jar stylechecker-backend-1.0-SNAPSHOT.jar server stylechecker-config.yml`

Configuration can be overridden using system properties.

For e.g. `java -Ddw.server.connector.port=9000 -jar stylechecker-backend-1.0-SNAPSHOT.jar server stylecheker-config.yml`

Once running the service will start a HTTP server listening on the specified port and path

By Default:

    The front end assets can be accessed on `http://localhost:8888/stylechecker`

    The HTTP api can be accessed on `http://localhost:8888/stylechecker/api/check`

    The Dropwizard admin page can be accessed on `http://localhost:8888/admin`


To use to CLI:

1. Create a configuration file and example can be found in `src\main\resources\stylechecker-config.yml`

2. Run the following to start the server `java -jar stylechecker-backend-1.0-SNAPSHOT.jar checker -i <INPUT_DIR> -o <OUTPUT_DIR> --conf stylechecker-config.yml`